packer {
  required_plugins {
    docker = {
      version = ">= 0.0.7"
      source  = "github.com/hashicorp/docker"
    }
  }
}

source "docker" "python" {
  image  = "python:3-alpine"
  commit = true
  changes = [
    "ENTRYPOINT /ansible_venv/bin/ansible-pull -i localhost, -U \"$ANSIBLE_GIT_REPO\" \"$ANSIBLE_PLAYBOOK\""
  ]
}

build {
  name = "ansible-pull"
  sources = [
    "source.docker.python"
  ]

  provisioner "shell" {
    environment_vars = [
      "VENV=/ansible_venv",
    ]
    inline = [
      "echo Installing git",
      "apk add git",
      "echo Installing ansible",
      "python -m venv $VENV",
      "$VENV/bin/pip install ansible",
      "ln -fs $VENV/bin/ansible-pull /usr/local/bin",
    ]
  }

  post-processor "docker-tag" {
    repository = "local"
    tags       = ["0.1", "latest"]
  }
}
